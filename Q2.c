#include <stdio.h>
#include <string.h>

int main() {
	
	char ch, str[500];
	int count = 0;
	
	printf("Enter a string: ");
	fgets(str, sizeof(str), stdin);
	
	printf("Character you need the frequency of: ");
	scanf("%c", &ch);
	
	for(int i = 0; str[i] != '\0'; i++) {
		if(ch == str[i])
			++count;
	}
	
	printf("Frequency of %c = %d", ch, count);
	
	return 0;
}