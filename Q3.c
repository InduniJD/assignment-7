#include<stdio.h>

void main()
{
	int a[3][3], b[3][3], c[3][3]={0}, d[3][3]={0};
	int i,j,k,m,n,p,q;
	printf("\nEnter number of rows in the first matrix: ");
	scanf("%d", &m);
	printf("Enter number of columns in the first matrix: ");
	scanf("%d", &n);
	
	printf("\nEnter number of rows in the second matrix: ");
	scanf("%d", &p);
	printf("Enter number of columns in the second matrix: ");
	scanf("%d", &q);
	
	if(m!=p || n!=q) {
	
		printf("Cannot add the two matrixes!!!\n");
		return;
	} else if(n!=p) {
		printf("Cannot multiply the two the matrixes!!!\n");
		return;
	} else {
		
		printf("Enter elements of the first matrix : \n");
		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				scanf("%d", &a[i][j]);
			
		printf("Enter elements of the second matrix : \n");
		for(i=0;i<p;i++)
			for(j=0;j<q;j++)
				scanf("%d", &b[i][j]);
			
		//Addition
		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				c[i][j] = a[i][j] + b[i][j];
		printf("\nAfter adding the two matrixes:\n");
		
		for(i=0;i<m;i++) {
			for(j=0;j<n;j++)
				printf("%d ", c[i][j]);
			printf("\n");
		}
		
		//Multiplication
		for(i=0;i<m;i++)
			for(j=0;j<q;j++)
				for(k=0;k<p;k++)
					d[i][j] += a[i][k]*b[k][j];
		printf("\nAfer multiplying the two matixes:\n");
		
		for(i=0;i<m;i++) {
			for(j=0;j<q;j++)
				printf("%d ", d[i][j]);
			printf("\n");
		}
	}
	getchar();
}