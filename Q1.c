#include <stdio.h>
#include <string.h>

int main() {
	
	char str[200];
	int i, len;
	
	printf("Enter a sentence: \n");
	gets(str);
	
	len = strlen(str);
	
	printf("\nAfter reversing: \n");
	
	for(i=len - 1; i>=0; i--) {
		printf("%c", str[i]);
	}
	
	return 0;
}